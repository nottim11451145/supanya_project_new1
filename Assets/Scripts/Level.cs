﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class Level : MonoBehaviour
{
    public static Level Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GameManager.Instance.enemySpaceshipHp += 30;

       
    }
    
    public void ResetLevel()
    {
        GameManager.Instance.enemySpaceshipHp = 100;

        uiManager.level = 0;
    }


}
